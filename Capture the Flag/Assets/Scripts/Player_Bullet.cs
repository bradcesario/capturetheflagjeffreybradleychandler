﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// @Author: Bradley Cesario, 10/26/2017
/// The purpose of class Player_Bullet is to account for damage done by the
/// bullet prefab to the player it hits.
/// </summary>
public class Player_Bullet : MonoBehaviour
{
    public float damage;
    //----------------NOTICE!!!-----------------------
    //Need to account for player health into damage.
    //------------------------------------------------
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            //Destroy bullet upon collision with player.
            Destroy(gameObject);
        }

    }
}
